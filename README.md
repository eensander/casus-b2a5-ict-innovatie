# B2A5 ICT Innovatie Community

## Getting Started

0\. Clone this repository

```
git clone git@bitbucket.org:eensander/casus-b2a5-ict-innovatie.git
cd casus-b2a5-ict-innovatie/
```

1\. Install composer and node dependencies

```
composer install
cp .env.example .env
php artisan key:generate
```

```
npm install
```

2\. Edit .env and fill in the database details accordingly, and add the following to the bottom:

```
GITEA_URL=http://13.81.40.142:3000
GITEA_ACCESS_TOKEN=0922013c8894f4aa5f67a48e390af80eb1dfd270
```

3\. Run migations into local database

```
php artisan migrate:fresh --seed
```

4\. Run the local development webserver

```
php artisan serve
```


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
