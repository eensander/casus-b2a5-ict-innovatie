<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

use Avency\Gitea;

$giteaClient = new Gitea\Client(
    env('GITEA_URL'),
    [
        'type' => Gitea\Client::AUTH_TOKEN,
        'auth' => env('GITEA_ACCESS_TOKEN')
    ]
);
$giteaClient->Miscellaneous()->version(); // to throw error when gitea is not available

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'username' => $faker->username,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('password'), // password
        'remember_token' => Str::random(10),
    ];
});

$factory->afterCreating(User::class, function ($user, $faker) use ($giteaClient) {
    $giteaUser = $giteaClient->admin()->createUser(
        $user->username,
        'password', // will be added on account creation.
        $user->email,
        null, // full name
        null, // loginname (?)
        false, // mustChangePassword
        true // sendNotify
    );

    var_dump($giteaUser);

    $user->gitea_id = $giteaUser['id'];
    $user->save();


    for ($i = 0; $i < 10; $i ++)
    {

        $message = new App\Message;

        $message->destination_user_id = 1;
        $message->source_user_id = $user->id;
        $message->content = $faker->text;

        $message->save();

    }

    // $user->outbox_messages()->create([
    //     'destination_user_id' => 1,
    //     'content' => $faker->text
    // ]);
    //
});
