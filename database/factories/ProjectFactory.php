<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Project;
use Faker\Generator as Faker;

use Faker\Provider\Internet;


$factory->define(Project::class, function (Faker $faker) {
    return [
        'slug' => $faker->unique()->slug,
        'title' => $faker->sentence(4),
        'short_description' => $faker->text(200),
        'repository_url' => $faker->url,
    ];
});
