<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\InviteToken;
use Faker\Generator as Faker;

use \Carbon\Carbon as Carbon;

$factory->define(InviteToken::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'used' => false,
        'expires_at' => Carbon::now()->addWeeks(2) // valid for (next) two weeks
    ];
});
