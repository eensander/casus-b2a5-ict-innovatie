<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@localhost',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'username' => 'moderator',
            'email' => 'moderator@localhost',
            'password' => Hash::make('password'),
        ]);
    }
}
