<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'parent_id' => null,
                'title' => 'Announcements',
                'slug' => Str::slug('announcements', '-'),
                'body' => 'Stay in touch with the latest announcements on our platform! Make sure you check this out once in a while.',
                'order' => 1,
                'image_url' => asset('images/discussions/categories/announcements.svg'),
                'is_closed' => true,
            ],
            [
                'parent_id' => null,
                'title' => 'Questions',
                'slug' => Str::slug('questions', '-'),
                'body' => 'Do you have any questions? This is the perfect place to ask them!',
                'order' => 2,
                'image_url' => asset('images/discussions/categories/questions.svg'),
                'is_closed' => false,
            ],
        ]);
    }
}
