<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // https://docs.spatie.be/laravel-permission/v3/basic-usage/basic-usage/

        /**
         * Role: Guest
         * - Can't do anything, except register, login and password reset which is basically checking if auth or not
         */

        /**
         * Role: Default
         * - Anything you can do while logged in where additional permissions are not required (?)
         */
        Role::create(['name' => 'default']);

        /**
         * Role: Moderator
         */
        Role::create(['name' => 'moderator'])->syncPermissions(
            Role::findByName('default')->permissions()->get(), // inherit default permissions (if any)

            Permission::create(['name' => 'discussion.lock']),
            Permission::create(['name' => 'discussion.pin']),
            Permission::create(['name' => 'discussion.update']),
            Permission::create(['name' => 'discussion.delete']),
            Permission::create(['name' => 'discussion.reply']),
            Permission::create(['name' => 'discussion.create']), // post in locked category
            Permission::create(['name' => 'category.lock']) // lock a category
        );

        /**
         * Role: Administrator
         * - Access admin panel. basically a ->hasRole('admin') on the /admin rule is sufficient
         */
        Role::create(['name' => 'admin'])->syncPermissions(
            Role::findByName('moderator')->permissions()->get(), // inherit moderator permissions

            Permission::create(['name' => 'admin'])
        );

        // assigning roles
        $user_admin = App\User::where('username', 'admin')->first();
        $user_admin->assignRole('admin');

        // verbose information
        echo " Permissions for 'Admin':\n";
        foreach($user_admin->getAllPermissions() as $permission)
        {
            echo ' - ' . $permission->name . PHP_EOL;
        }

        $user_mod = App\User::where('username', 'moderator')->first();
        $user_mod->assignRole('moderator');

        // verbose information
        echo " Permissions for 'Moderator':\n";
        foreach($user_mod->getAllPermissions() as $permission)
        {
            echo ' - ' . $permission->name . PHP_EOL;
        }

    }
}
