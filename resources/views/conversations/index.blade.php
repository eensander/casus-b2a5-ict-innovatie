@extends('layouts.app')

@section('content')

    {{-- <h1 class="text-3xl font-medium text-gray-800 mb-4">Conversation with <strong>{{ $destination_user->username }}</strong></h1> --}}

    <conversation-window :base-route="{{ json_encode(route('conversation.index')) }}" :user-id="{{ Auth::user()->id }}" ></conversation-window>

@endsection
