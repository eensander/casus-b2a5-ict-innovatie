@extends('layouts.app')

@section('content')
    <div class="flex items-center justify-between pb-3 border-b border-gray-300 mb-8">
        <h1 class="text-4xl text-gray-900 font-bold uppercase">{{ $user->username }}</h1>
    </div>
    <div class="flex">
        <div class="w-9/12">
            <div class="bg-white p-8 rounded-md shadow-lg">
                <div>
                    <img
                        src="{{ $user->avatar() }}"
                        alt="{{ $user->username }}'s profile image"
                        class="rounded-2"
                    />
                </div>
            </div>
        </div>
    </div>
@endsection
