@extends ('layouts.boilerplate')

@section ('base')
    <div class="w-screen h-screen">
        <div class="w-full h-full flex items-center md:items-start justify-between overflow-hidden">
            <div class="w-full h-full md:max-w-lg mx-auto mx-0 overflow-auto">
                <div class="p-12 pt-24">
                    <div class="-mt-12">
                        <a
                            href="/"
                            class="inline-block mb-16"
                        >
                            <img
                                src="/images/logo.svg"
                                alt="Logo"
                                class="h-12 w-auto"
                            />
                        </a>
                    </div>
                    @yield ('content')
                </div>
            </div>
            <div class="hidden md:block relative h-full flex-1">
                <div class="absolute w-full h-full bg-blue-500 opacity-50"></div>
                <img
                    src="https://images.unsplash.com/photo-1522071820081-009f0129c71c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80"
                    alt="People working on a project"
                    class="w-full h-full object-cover"
                >
            </div>
        </div>
    </div>
@endsection
