@extends ('layouts.boilerplate')

@section('app-notification')
@if (Session::get('status'))
    <app-notification
            message="{{ Session::get('status') }}"

            @if (Session::get('type'))
            type="{{ Session::get('type') }}"
            @endif

            @if (Session::get('duration'))
            duration="{{ Session::get('duration') }}"
            @endif
    ></app-notification>
@endif
@endsection

@section ('base')
    <div class="flex h-screen w-screen">
        <aside class="hidden lg:block w-full max-w-xs px-8 py-4 bg-white border-r border-gray-300">
            <div class="h-16 -mt-4 flex items-center">
                <a
                        href="/"
                        class="inline-block"
                >
                    <img
                        src="{{ asset('images/logo.svg') }}"
                        alt="Logo"
                        class="h-10"
                    >
                </a>
            </div>
            <nav class="mt-6">
                <div>
                    <h2 class="text-gray-500 text-sm font-bold tracking-wider uppercase">
                        Menu
                    </h2>
                    <div class="mt-4">
                        <a href="{{ route('home') }}" class="nav-link {{ Request::segment(1) === null ? 'active' :
                            null }}"
                        >
                            <div>
                                <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none">
                                    <path d="M3 12L5 10M5 10L12 3L19 10M5 10V20C5 20.5523 5.44772 21 6 21H9M19 10L21
                                    12M19 10V20C19 20.5523 18.5523 21 18 21H15M9 21C9.55228 21 10 20.5523 10 20V16C10
                                     15.4477 10.4477 15 11 15H13C13.5523 15 14 15.4477 14 16V20C14 20.5523 14.4477 21
                                      15 21M9 21H15" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                          stroke-linejoin="round"/>
                                </svg>
                            </div>
                            <span class="ml-3">
                                Home
                            </span>
                        </a>
                        <a href="{{ env('GITEA_URL') }}" target="_blank" class="nav-link mt-2 {{ (Request::segment(1) ===
                            'projects') ? 'active' : null }}"
                        >
                            <div>
                                <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none">
                                    <path
                                        d="M3 7V17C3 18.1046 3.89543 19 5 19H19C20.1046 19 21 18.1046 21 17V9C21 7.89543 20.1046 7 19 7H13L11 5H5C3.89543 5 3 5.89543 3 7Z"
                                        stroke="currentColor"
                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </div>
                            <span class="ml-3">
                                Projects
                            </span>
                        </a>
                        <a href="{{ route('discussion.index') }}" class="nav-link mt-2 {{ Request::segment(1) ===
                        'discussions' ? 'active' : null }}"
                        >
                            <div>
                                <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none">
                                    <path
                                        d="M8 12H8.01M12 12H12.01M16 12H16.01M21 12C21 16.4183 16.9706 20 12 20C10.4607 20 9.01172 19.6565 7.74467 19.0511L3 20L4.39499 16.28C3.51156 15.0423 3 13.5743 3 12C3 7.58172 7.02944 4 12 4C16.9706 4 21 7.58172 21 12Z"
                                        stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                                </svg>
                            </div>
                            <span class="ml-3">
                                Discussions
                            </span>
                        </a>
@role('admin')
                        <div class="border-b border-gray-400 mt-3"></div>
                        <a href="{{ route('admin.users.index') }}" class="flex items-center -ml-4 -mr-4 px-4 py-2 mt-2 text-gray-500 rounded-md
                        hover:text-blue-700 hover:bg-blue-100">
                            <div>
                            </div>
                            <span class="font-medium ml-3">
                                Admin
                            </span>
                        </a>
@endrole
                    </div>
                    <div class="mt-6">
                        @yield ('sidebar')
                    </div>
                </div>
            </nav>
        </aside>
        <div class="flex-1 overflow-auto">
            <header class="bg-white h-16 w-full px-12 border-b border-gray-300">
                <div class="w-full h-full flex items-center justify-between">
                    <div class="relative flex-1">
                        <label for="search" class="absolute inset-y-0 text-gray-600 pl-4 flex items-center">
                            <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none">
                                <path
                                        d="M21 21L15 15M17 10C17 13.866 13.866 17 10 17C6.13401 17 3 13.866 3 10C3 6.13401 6.13401 3 10 3C13.866 3 17 6.13401 17 10Z"
                                        stroke="currentColor"
                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </label>
                        <input
                            type="text"
                            id="search"
                            name="search"
                            placeholder="Search"
                            class="input w-full max-w-sm pl-12"
                        >
                    </div>
                    <div class="ml-10 flex items-center">
                        <a href="{{ route('conversation.index') }}" class="mr-2 p-2 rounded-full text-gray-600 hover:text-blue-700 hover:bg-blue-100
                        focus:text-blue-700 focus:bg-blue-100 focus:outline-none focus:shadow-outline">
                            <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none">
                                <path
                                    d="M3 8L10.8906 13.2604C11.5624 13.7083 12.4376 13.7083 13.1094 13.2604L21 8M5 19H19C20.1046 19 21 18.1046 21 17V7C21 5.89543 20.1046 5 19 5H5C3.89543 5 3 5.89543 3 7V17C3 18.1046 3.89543 19 5 19Z"
                                    stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round"/>
                            </svg>
                        </a>
                        <a class="mr-6 p-2 rounded-full text-gray-600 hover:text-blue-700 hover:bg-blue-100
                        focus:text-blue-700 focus:bg-blue-100 focus:outline-none focus:shadow-outline">
                            <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none">
                                <path
                                        d="M15 17H20L18.5951 15.5951C18.2141 15.2141 18 14.6973 18 14.1585V11C18 8.38757 16.3304 6.16509 14 5.34142V5C14 3.89543 13.1046 3 12 3C10.8954 3 10 3.89543 10 5V5.34142C7.66962 6.16509 6 8.38757 6 11V14.1585C6 14.6973 5.78595 15.2141 5.40493 15.5951L4 17H9M15 17V18C15 19.6569 13.6569 21 12 21C10.3431 21 9 19.6569 9 18V17M15 17H9"
                                        stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                            </svg>
                        </a>
                        <app-profile-dropdown
                                username="{{ auth()->user()->username }}"
                                avatar-url="{{ auth()->user()->avatar() }}"
                                profile-url="/"
                        >
                            <div>

                            </div>
                            <a href="#" class="block px-2 py-2 hover:bg-gray-100">
                                My Profile
                            </a>
                            <a href="{{ route('settings.') }}" class="block px-2 py-2 hover:bg-gray-100">
                                Settings
                            </a>
                            <form action="{{ route('logout') }}" method="POST" class="w-full">
                                @csrf

                                <button type="submit" class="block w-full text-left px-2 py-2 hover:bg-gray-100">
                                    Log Out
                                </button>
                            </form>
                        </app-profile-dropdown>
                    </div>
                </div>
            </header>
            @if(View::hasSection('extra_content_aside'))
                @yield('extra_content_h1')
                <main class="px-8 flex">
                    @yield('extra_content_aside')
                    <div class="container px-5 pb-6 overflow-auto">
                        @yield ('app-notification')
                        @yield ('content')
                </div>
            </main>
@else
            <main class="mx-auto overflow-auto">
                <div class="relative container mx-auto px-12 pt-6 pb-24">
                    @yield ('app-notification')
                    @yield ('content')
                </div>
            </main>
@endif

        </div>
    </div>
@endsection
