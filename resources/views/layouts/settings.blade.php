@extends ('layouts.app')


@section('extra_content_h1')
    <div class="flex items-center justify-between py-2 pl-6 border-b border-gray-idk mb-2">
        <h1 class="text-4xl text-gray-900 font-bold">Settings</h1>
    </div>
@endsection

@section('extra_content_aside')

<div class="second-aside ">

    <p>General</p>

    <ul>

        <li class="">
            <a href="{{ route('settings.profile') }}" class="{{ Route::currentRouteName() == 'settings.profile' ? 'active' : null }}">Profile</a>
        </li>

        <li class="">
            <a href="{{ route('settings.profile') }}">Account</a>
        </li>

        <li class="">
            <a href="{{ route('settings.profile') }}">Notifications</a>
        </li>

        <li class="">
            <a href="{{ route('settings.password') }}" class="{{ Route::currentRouteName() == 'settings.password' ? 'active' : null }}">Password</a>
        </li>

        <li class="">
            <a href="{{ route('settings.profile') }}" class="red">Deactivate</a>
        </li>

    </ul>

</div>

@endsection
