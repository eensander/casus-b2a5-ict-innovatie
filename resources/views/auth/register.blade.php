@extends('layouts.auth')

@section('content')
    <div>
        <h1 class="text-3xl font-bold text-gray-900">
            {{ $register_enabled ? 'Welcome!' : 'Failure' }}
        </h1>

        <p class="mt-2 text-base text-gray-700">
            {{ $header_message }}
        </p>
        @error ('invite')
        <p class="mt-2 font-medium text-sm text-red-400">
            {{ $message }}
        </p>
        @enderror
    </div>
    @if ($register_enabled)
    <div class="mt-12 text-left">
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <input type="hidden" name="invite_token" value="{{ $invite->uuid }}" />

            <div>
                <label
                    for="username"
                    class="text-base text-gray-800 font-bold"
                >
                    Username
                </label>

                <input
                    type="text"
                    id="username"
                    name="username"
                    value="{{ old('username') }}"
                    class="input w-full mt-2 @error('username') error @enderror"
                    required
                    autocomplete="username"
                    autofocus
                >

                @error ('username')
                <p class="mt-2 font-medium text-sm text-red-400">
                    {{ $message }}
                </p>
                @enderror
            </div>
            <div class="mt-6">
                <label
                    for="email"
                    class="text-base text-gray-800 font-bold"
                >
                    Email address
                </label>

                <input
                    type="text"
                    id="email"
                    name="email"
                    value="{{ $invite->email }}"
                    class="input w-full mt-2 bg-gray-200 cursor-not-allowed @error('email') error @enderror"
                    required autocomplete="email"
                    readonly
                >

                @error ('email')
                <p class="mt-2 font-medium text-sm text-red-400">
                    {{ $message }}
                </p>
                @enderror
            </div>
            <div class="mt-6">
                <label
                    for="password"
                    class="text-base text-gray-800 font-bold"
                >
                    Password
                </label>

                <input
                    type="password"
                    id="password"
                    name="password"
                    class="input w-full mt-2 @error('password') error @enderror"
                    autocomplete="new-password"
                    required
                >

                @error ('password')
                <p class="mt-2 font-medium text-sm text-red-400">
                    {{ $message }}
                </p>
                @enderror
            </div>
            <div class="mt-6">
                <label
                    for="password-confirm"
                    class="text-base text-gray-800 font-bold"
                >
                    Confirm password
                </label>

                <input
                    type="password"
                    id="password_confirmation"
                    name="password_confirmation"
                    class="input w-full mt-2 @error('password_confirmation') error @enderror"
                    autocomplete="new-password"
                    required
                >

                @error ('password_confirmation')
                <p class="mt-2 font-medium text-sm text-red-400">
                    {{ $message }}
                </p>
                @enderror
            </div>
            <div class="mt-6">
                <button
                    type="submit"
                    class="btn w-full"
                >
                    Register
                </button>
            </div>
        </form>
    </div>
    @endif
@endsection
