@extends('layouts.auth')

@section('content')
    <div>
        <h1 class="text-3xl font-bold text-gray-900">
            It happens!
        </h1>

        <p class="mt-2 text-base text-gray-700">
            Enter your email below and we will send you a link to reset your password.
        </p>
    </div>
    <div class="mt-12 text-left">
        @if (session('status'))
            <p class="bg-green-200 text-green-700 px-3 py-2 mb-4 rounded-md">
                {{ session('status') }}
            </p>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div>
                <label
                    for="email"
                    class="text-base text-gray-800 font-bold"
                >
                    Email address
                </label>

                <input
                    type="text"
                    id="email"
                    name="email"
                    value="{{ old('email') }}"
                    class="input w-full mt-2 @error('email') error @enderror"
                    required autocomplete="email"
                    autofocus
                >

                @error ('email')
                <p class="mt-2 font-medium text-sm text-red-400">
                    {{ $message }}
                </p>
                @enderror
            </div>
            <div class="mt-6">
                <button
                    type="submit"
                    class="btn w-full"
                >
                    Send password reset link
                </button>
            </div>
        </form>
    </div>
@endsection
