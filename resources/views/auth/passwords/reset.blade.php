@extends('layouts.auth')

@section('content')
    <div>
        <h1 class="text-3xl font-bold text-gray-900">
            ???
        </h1>

        <p class="mt-2 text-base text-gray-700">
            Please fill out the form below to reset your password.
        </p>
    </div>
    <div class="mt-12 text-left">
        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div>
                <label
                    for="email"
                    class="text-base text-gray-800 font-bold"
                >
                    Email address
                </label>

                <input
                    type="text"
                    id="email"
                    name="email"
                    value="{{ $email ?? old('email') }}"
                    class="input w-full mt-2 @error('email') error @enderror"
                    required autocomplete="email"
                >

                @error ('email')
                <p class="mt-2 font-medium text-sm text-red-400">
                    {{ $message }}
                </p>
                @enderror
            </div>
            <div class="mt-6">
                <label
                    for="password"
                    class="text-base text-gray-800 font-bold"
                >
                    Password
                </label>

                <input
                    type="password"
                    id="password"
                    name="password"
                    class="input w-full mt-2 @error('password') error @enderror"
                    autocomplete="new-password"
                    required
                >

                @error ('password')
                <p class="mt-2 font-medium text-sm text-red-400">
                    {{ $message }}
                </p>
                @enderror
            </div>
            <div class="mt-6">
                <label
                    for="password-confirm"
                    class="text-base text-gray-800 font-bold"
                >
                    Confirm password
                </label>

                <input
                    type="password"
                    id="password_confirmation"
                    name="password_confirmation"
                    class="input w-full mt-2 @error('password_confirmation') error @enderror"
                    autocomplete="new-password"
                    required
                >

                @error ('password_confirmation')
                <p class="mt-2 font-medium text-sm text-red-400">
                    {{ $message }}
                </p>
                @enderror
            </div>
            <div class="mt-6">
                <button
                    type="submit"
                    class="btn w-full"
                >
                    Reset Password
                </button>
            </div>
        </form>
    </div>
@endsection
