@extends ('layouts.auth')

@section ('content')
    <div>
        <h1 class="text-3xl font-bold text-gray-900">
            Welcome back!
        </h1>

        <p class="mt-2 text-base text-gray-700">
            Enter your credentials to access our platform.
        </p>
    </div>
    <div class="mt-12 text-left">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <label
                    for="email"
                    class="text-base text-gray-800 font-bold"
                >
                    Email address
                </label>

                <input
                    type="text"
                    id="email"
                    name="email"
                    value="{{ old('email') }}"
                    class="input w-full mt-2 @error('email') error @enderror"
                    required autocomplete="email"
                    autofocus
                >

                @error ('email')
                <p class="mt-2 font-medium text-sm text-red-400">
                    {{ $message }}
                </p>
                @enderror
            </div>
            <div class="mt-6">
                <label
                    for="password"
                    class="text-base text-gray-800 font-bold"
                >
                    Password
                </label>

                <input
                    type="password"
                    id="password"
                    name="password"
                    class="input w-full mt-2 @error('password') error @enderror"
                >

                @error ('password')
                <p class="mt-2 font-medium text-sm text-red-400">
                    {{ $message }}
                </p>
                @enderror
            </div>
            <div class="mt-6">
                <button
                    type="submit"
                    class="btn w-full"
                >
                    Log In
                </button>
            </div>
        </form>
        <div class="mt-6 relative">
            <div class="absolute inset-0 flex items-center">
                <div class="w-full border-t border-gray-300"></div>
            </div>
            <div class="relative flex justify-center text-sm leading-5">
                <a
                    href="{{ route('password.request') }}"
                    class="btn-tertiary bg-gray-100 text-sm px-2 font-normal"
                >
                    Forgot your password?
                </a>
            </div>
        </div>
    </div>
@endsection
