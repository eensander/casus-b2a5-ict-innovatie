<div class="flex items-start w-full mb-8">
    <div class="relative">
        <img
                src="{{ $reply->user->avatar()  }}"
                alt="{{ $reply->user->username }}'s avatar"
                class="h-10 w-10 rounded-full object-cover @if ($reply->isBestReply()) border-4 border-green-400 @endif"
        >

        @if ($reply->isBestReply())
            <div class="absolute flex justify-center items-center top-0 right-0 h-4 w-4 text-green-100 bg-green-400
                        rounded-full"
            >
                <svg class="w-4 h-4" viewBox="0 0 20 20" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M16.7071 5.29289C17.0976 5.68342 17.0976 6.31658
                     16.7071 6.70711L8.70711 14.7071C8.31658 15.0976 7.68342 15.0976 7.29289 14.7071L3.29289
                     10.7071C2.90237 10.3166 2.90237 9.68342 3.29289 9.29289C3.68342 8.90237 4.31658 8.90237
                     4.70711 9.29289L8 12.5858L15.2929 5.29289C15.6834 4.90237 16.3166 4.90237 16.7071 5.29289Z"
                          fill="currentColor"/>
                </svg>
            </div>
        @endif
    </div>
    <div class="ml-6 flex-1">
        <div class="flex items-center">
            <a href="#" class="block text-gray-800 font-bold leading-none">
                {{ $reply->user->username }}
            </a>

            <span class="text-sm text-gray-600 ml-2">
                {{ $reply->created_at->diffForHumans() }}
            </span>
        </div>

        <p class="mt-2">
            {{ $reply->body }}
        </p>

        <div class="mt-2 flex items-center">
            <div class="flex items-center text-gray-700">
                @if (!$reply->likers->contains(auth()->user()))
                    <form action="{{ route('discussion.reply.like.store', [$discussion->category, $discussion,
                    $reply]) }}" method="POST">
                        @csrf

                        <button type="submit" class="flex justify-center items-center h-6 w-6
                        rounded-full focus:outline-none focus:shadow-outline"
                        >
                            <svg class="h-4 w-4" viewBox="0 0 24 24" fill="none">
                                <path d="M4.31802 6.31802C2.56066 8.07538 2.56066 10.9246 4.31802 12.682L12.0001 20.364L19.682 12.682C21.4393 10.9246 21.4393 8.07538 19.682 6.31802C17.9246 4.56066 15.0754 4.56066 13.318 6.31802L12.0001 7.63609L10.682 6.31802C8.92462 4.56066 6.07538 4.56066 4.31802 6.31802Z"
                                      stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                      stroke-linejoin="round"/>
                            </svg>
                        </button>
                    </form>
                @else
                    <form action="{{ route('discussion.reply.like.delete', [$discussion->category, $discussion,
                    $reply]) }}" method="POST">
                        @csrf
                        @method ('DELETE')

                        <button type="submit" class="flex justify-center items-center h-6 w-6
                        rounded-full focus:outline-none focus:shadow-outline text-blue-100 bg-blue-500"
                        >
                            <svg class="h-4 w-4" viewBox="0 0 24 24" fill="none">
                                <path d="M4.31802 6.31802C2.56066 8.07538 2.56066 10.9246 4.31802 12.682L12.0001 20.364L19.682 12.682C21.4393 10.9246 21.4393 8.07538 19.682 6.31802C17.9246 4.56066 15.0754 4.56066 13.318 6.31802L12.0001 7.63609L10.682 6.31802C8.92462 4.56066 6.07538 4.56066 4.31802 6.31802Z"
                                      stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                      stroke-linejoin="round"/>
                            </svg>
                        </button>
                    </form>
                @endif
                <span class="ml-2 text-sm font-medium">
                    {{ $reply->likes->count() }} {{ \Str::plural('like',
                    $reply->likes->count()) }}
                </span>
            </div>
            @if (!$reply->isBestReply() && (auth()->user()->id === $discussion->user->id || auth()->user()->can('discussion.lock')))
                <form action="{{ route('discussion.reply.best.store', [$discussion->category, $discussion, $reply]) }}"
                      method="POST" class="ml-4 text-sm font-medium
                text-gray-700">
                    @csrf

                    <button type="submit" class="font-medium">
                        Best reply
                    </button>
                </form>
            @endif
        </div>

        @if(!$discussion->is_locked || auth()->user()->can('discussion.reply'))
            <form action="{{ route('discussion.reply.store', [$discussion->category, $discussion, $reply]) }}"
                  method="POST" class="mt-2">
                @csrf

                <textarea
                    id="body"
                    name="body"
                    rows="3"
                    class="w-full input border-dashed"
                ></textarea>

                <button type="submit" class="btn mt-2">
                    Reply
                </button>
            </form>
        @endif

        <div class="mt-4">
            @foreach ($reply->replies as $subReply)
                @include ('discussions.partials.sub_reply')
            @endforeach
        </div>
    </div>
</div>
