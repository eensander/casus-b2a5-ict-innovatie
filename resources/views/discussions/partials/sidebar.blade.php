<div class="w-full max-w-xs">
    <div>
        @isset ($currentCategory)
            @can ('create', ['App/Discussion', $currentCategory])
                <a
                        href="{{ route('discussion.create', $currentCategory ?? '') }}"
                        class="btn w-full text-center mb-6"
                >
                    Start a discussion
                </a>
            @endcan
        @endisset

        <div>
            <h3 class="text-lg font-medium text-gray-700">
                Most recent discussions
            </h3>
            <p class="mt-2 text-sm text-gray-600">
                The discussions people are currently talking about, why don't you join them?
            </p>
        </div>
    </div>
</div>