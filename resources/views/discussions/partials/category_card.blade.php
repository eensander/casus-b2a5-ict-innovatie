<div class="bg-white px-10 py-8 mb-6 rounded-md shadow-lg">
    <div class="flex justify-between items-center">
        <div class="flex items-start">
            <div class="bg-blue-100 rounded-md flex-shrink-0">
                <img
                    src="{{ $category->image_url }}"
                    alt="{{ $category->title }} image"
                    class="h-32 w-32 min-w-full rounded-md object-cover"
                />
            </div>
            <div class="ml-8 flex-shrink-1">
                <a href="{{ $category->path() }}">
                    <h2 class="text-3xl text-gray-800 font-bold">{{ $category->title }}</h2>
                </a>
                <p class="mt-1 text-gray-700 h-full w-full max-w-lg">{{ $category->body }}</p>
            </div>
        </div>
        <div class="hidden flex-shrink-1 2xl:ml-4 2xl:block 2xl:w-56 2xl:border-l 2xl:border-gray-300">
            <div class="pl-3 flex items-center border-b border-gray-300 pb-1">
                <div>
                    <span class="block text-lg text-gray-600 font-bold leading-none">
                        {{ $category->discussions->count() }}
                    </span>
                    <span class="text-xs text-gray-500 font-normal">
                        discussions
                    </span>
                </div>

                <div class="ml-4">
                    <span class="block text-lg text-gray-600 font-bold leading-none">
                        {{ $category->replies->count() }}
                    </span>
                    <span class="text-xs text-gray-500 font-normal">
                        replies
                    </span>
                </div>
            </div>
            <div class="pl-3">
                <span class="text-xs text-gray-500 leading-none">
                    Latest discussion
                </span>
                @if ($category->latestDiscussion)
                    <a href="{{ $category->latestDiscussion->path() }}" class="block text-sm
                    text-gray-600 font-bold truncate">
                        {{ $category->latestDiscussion->title }}
                    </a>
                @else
                    <p class="text-sm text-gray-600 font-bold truncate">
                        No discussions yet
                    </p>
                @endif

            </div>
        </div>
    </div>
</div>
