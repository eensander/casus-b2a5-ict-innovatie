<div class="flex items-start mb-4">
    <div>
        <img
                src="{{ $subReply->user->avatar() }}"
                alt="{{ $subReply->user->username }}'s avatar"
                class="w-8 h-8 rounded-full object-cover"
        >
    </div>
    <div class="ml-4 flex-1">
        <div class="flex items-center">
            <a href="#" class="block text-gray-800 font-bold leading-none">
                {{ $subReply->user->username }}
            </a>

            <span class="text-sm text-gray-600 ml-2">
                {{ $subReply->created_at->diffForHumans() }}
            </span>
        </div>

        <p class="mt-2">
            {{ $subReply->body }}
        </p>

        <div class="mt-2 flex items-center">
            <div class="flex items-center text-gray-700">
                @if (!$subReply->likers->contains(auth()->user()))
                    <form action="{{ route('discussion.reply.like.store', [$discussion->category, $discussion,
                    $subReply]) }}" method="POST">
                        @csrf

                        <button type="submit" class="flex justify-center items-center h-6 w-6
                        rounded-full focus:outline-none focus:shadow-outline"
                        >
                            <svg class="h-4 w-4" viewBox="0 0 24 24" fill="none">
                                <path d="M4.31802 6.31802C2.56066 8.07538 2.56066 10.9246 4.31802 12.682L12.0001 20.364L19.682 12.682C21.4393 10.9246 21.4393 8.07538 19.682 6.31802C17.9246 4.56066 15.0754 4.56066 13.318 6.31802L12.0001 7.63609L10.682 6.31802C8.92462 4.56066 6.07538 4.56066 4.31802 6.31802Z"
                                      stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                      stroke-linejoin="round"/>
                            </svg>
                        </button>
                    </form>
                @else
                    <form action="{{ route('discussion.reply.like.delete', [$discussion->category, $discussion,
                    $subReply]) }}" method="POST">
                        @csrf
                        @method ('DELETE')

                        <button type="submit" class="flex justify-center items-center h-6 w-6
                        rounded-full focus:outline-none focus:shadow-outline text-blue-100 bg-blue-500"
                        >
                            <svg class="h-4 w-4" viewBox="0 0 24 24" fill="none">
                                <path d="M4.31802 6.31802C2.56066 8.07538 2.56066 10.9246 4.31802 12.682L12.0001 20.364L19.682 12.682C21.4393 10.9246 21.4393 8.07538 19.682 6.31802C17.9246 4.56066 15.0754 4.56066 13.318 6.31802L12.0001 7.63609L10.682 6.31802C8.92462 4.56066 6.07538 4.56066 4.31802 6.31802Z"
                                      stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                      stroke-linejoin="round"/>
                            </svg>
                        </button>
                    </form>
                @endif
                <span class="ml-2 text-sm font-medium">
                    {{ $subReply->likes->count() }} {{ \Str::plural('like',
                    $subReply->likes->count()) }}
                </span>
            </div>
        </div>
    </div>
</div>