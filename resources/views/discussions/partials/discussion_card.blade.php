<div class="flex justify-between items-center w-full bg-white px-8 py-4 rounded-md shadow mb-6">
    <div class="flex">
        <div class="relative mr-6">
            <img
                    src="{{ $discussion->user->avatar() }}"
                    alt="{{ $discussion->user->username }}"
                    class="h-12 w-12 rounded-full object-cover
                    @if ($discussion->is_pinned)
                            border-4 border-yellow-400
                    @elseif ($discussion->bestReply)
                            border-4 border-green-400
                    @endif"
            />

            @if ($discussion->is_pinned)
                <div class="absolute flex justify-center items-center top-0 right-0 h-4 w-4 text-yellow-100
                bg-yellow-400 rounded-full">
                    <svg class="w-3 h-3" viewBox="0 0 20 20" fill="currentColor">
                        <path d="M11 12h6v-1l-3-1V2l3-1V0H3v1l3 1v8l-3 1v1h6v7l1 1 1-1v-7z"/>
                    </svg>
                </div>
            @elseif ($discussion->bestReply)
                <div class="absolute flex justify-center items-center top-0 right-0 h-4 w-4 text-green-100 bg-green-400
                rounded-full">
                    <svg class="w-4 h-4" viewBox="0 0 20 20" fill="none">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M16.7071 5.29289C17.0976 5.68342 17.0976 6.31658
                         16.7071 6.70711L8.70711 14.7071C8.31658 15.0976 7.68342 15.0976 7.29289 14.7071L3.29289
                         10.7071C2.90237 10.3166 2.90237 9.68342 3.29289 9.29289C3.68342 8.90237 4.31658 8.90237
                         4.70711 9.29289L8 12.5858L15.2929 5.29289C15.6834 4.90237 16.3166 4.90237 16.7071 5.29289Z"
                              fill="currentColor"/>
                    </svg>
                </div>
            @endif
        </div>
        <div>
            <a href="{{ route('discussion.show', [$currentCategory, $discussion]) }}" class="inline-block">
                <h3 class="text-2xl font-bold text-gray-800 leading-none truncate">
                    {{ $discussion->title }}
                </h3>
            </a>

            <div class="text-gray-700">
                <span>
                    by
                    <a href="#" class="font-bold">
                        {{ $discussion->user->username }}
                    </a>
                </span>

                @isset ($discussion->latestReply)
                    <span class="mx-2">&bull;</span>

                    <span>
                        Last reply was
                        <span class="font-medium">
                            {{ $discussion->latestReply->created_at->diffForHumans() }}
                        </span>
                        ago by
                        <a href="#" class="font-bold">
                            {{ $discussion->latestReply->user->username }}
                        </a>
                    </span>
                @endisset
            </div>
        </div>
    </div>

    <div class="flex">
        <div>
            <span class="block text-lg text-gray-600 font-bold leading-none">
                {{ $discussion->likes->count() }}
            </span>
            <span class="text-xs text-gray-500 font-normal">
                likes
            </span>
        </div>
        <div class="ml-10">
            <span class="block text-lg text-gray-600 font-bold leading-none">
                {{ $discussion->replies->count() }}
            </span>
            <span class="text-xs text-gray-500 font-normal">
                replies
            </span>
        </div>
    </div>
</div>
