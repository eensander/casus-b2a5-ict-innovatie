<div>
    @include ('discussions.partials.breadcrumbs')

    <div class="flex items-center justify-between mt-4 pb-3 border-b border-gray-300">
        <h1 class="text-4xl text-gray-900 font-bold">{{ $title }}</h1>
        @isset ($searchEnabled)
            @if ($searchEnabled)
                <div class="relative w-full max-w-xs">
                    <label for="search_discussions" class="absolute inset-y-0 text-gray-600 pl-4 flex items-center">
                        <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none">
                            <path
                                d="M21 21L15 15M17 10C17 13.866 13.866 17 10 17C6.13401 17 3 13.866 3 10C3 6.13401 6.13401 3 10 3C13.866 3 17 6.13401 17 10Z"
                                stroke="currentColor"
                                stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </label>
                    <input
                        type="text"
                        id="search_discussions"
                        name="search_discussions"
                        placeholder="Search in discussions"
                        class="input w-full max-w-xs pl-12 bg-gray-300"
                    >
                </div>
            @endif
        @endisset
    </div>
</div>
