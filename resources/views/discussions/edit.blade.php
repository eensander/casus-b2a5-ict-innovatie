@extends ('layouts.app')

@section ('content')
    @include ('discussions.partials.header', [
        'title' => 'Edit a discussion',
        'searchEnabled' => false,
    ])

    <div class="mt-8 xl:flex xl:justify-between">
        <div class="w-full xl:w-9/12 xl:pr-12">
            <div class="max-w-4xl">
                <form method="POST" action="{{ route('discussion.update', [$discussion->category, $discussion]) }}">
                    @csrf
                    @method ('PATCH')

                    <div>
                        <input
                                id="title"
                                name="title"
                                placeholder="Enter a title"
                                value="{{ $discussion->title }}"
                                class="text-5xl text-gray-900 font-bold tracking-tight px-2 leading-none border-2
                            border-dashed border-gray-300 rounded-md bg-transparent"
                        />
                    </div>
                    <div class="flex items-start mt-6">
                        <div class="flex-shrink-0">
                            <img
                                    src="{{ $discussion->user->avatar() }}"
                                    alt="{{ $discussion->user->username }}'s avatar"
                                    class="h-12 w-12 rounded-full object-cover"
                            />
                        </div>
                        <div class="ml-6 flex-shrink-1 w-full">
                            <div class="flex items-center leading-none">
                                <a
                                        href="#"
                                        class="text-xl text-gray-800 font-bold"
                                >
                                    {{ $discussion->user->username }}
                                </a>
                                <span class="ml-2 text-gray-600">
                                    {{ $discussion->created_at->diffForHumans() }}
                                </span>
                            </div>

                            <div class="mt-6 w-full">
                                <textarea
                                        id="body"
                                        name="body"
                                        placeholder="Enter a body"
                                        class="px-2 bg-transparent border border-dashed border-gray-300 text-gray-700
                                    leading-relaxed w-full"
                                        rows="6"
                                >{{ $discussion->body }}</textarea>

                                @if ($errors->any())
                                    <ul class="mt-2">
                                        @foreach ($errors->all() as $error)
                                            <li class="text-red-400 italic">{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif

                                <button
                                        type="submit"
                                        class="btn mt-2"
                                >
                                    Save discussion
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
