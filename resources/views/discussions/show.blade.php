@extends ('layouts.app')

@section ('content')
    @include ('discussions.partials.breadcrumbs')

    <div class="mt-8 xl:flex xl:justify-between">
        <div class="w-full xl:w-9/12 xl:pr-12">
            <div class="max-w-4xl">
                <div>
                    <div>
                        <h1 class="text-5xl text-gray-900 font-bold tracking-tight leading-none">
                            {{ $discussion->title }}
                        </h1>
                        <div class="mt-2 flex items-center">
                            <div class="flex items-center mr-2">
                                @if(auth()->user()->can('discussion.lock'))
                                    @if (!$discussion->is_locked)
                                        <form action="{{ route('discussion.lock.store', [$discussion->category,
                                        $discussion]) }}" method="POST">
                                            @csrf
                                            @method ('POST')

                                            <button type="submit" class="text-sm text-gray-600">
                                                Lock
                                            </button>
                                        </form>
                                    @else
                                        <form action="{{ route('discussion.lock.destroy', [$discussion->category,
                                        $discussion]) }}" method="POST">
                                            @csrf
                                            @method ('DELETE')

                                            <button type="submit" class="text-sm text-gray-600">
                                                Unlock
                                            </button>
                                        </form>
                                    @endif
                                @endif
                                @if(auth()->user()->can('discussion.pin'))
                                    @if (!$discussion->is_pinned)
                                        <form action="{{ route('discussion.pin.store', [$discussion->category,
                                        $discussion]) }}" method="POST" class="ml-2">
                                            @csrf
                                            @method ('POST')

                                            <button type="submit" class="text-sm text-gray-600">
                                                Pin
                                            </button>
                                        </form>
                                    @else
                                        <form action="{{ route('discussion.pin.destroy', [$discussion->category,
                                        $discussion]) }}" method="POST" class="ml-2">
                                            @csrf
                                            @method ('DELETE')

                                            <button type="submit" class="text-sm text-gray-600">
                                                Unpin
                                            </button>
                                        </form>
                                    @endif
                                @endif
                            </div>

                            <div class="flex items-center mr-2">
                                @can ('update', $discussion)
                                    <a href="{{ route('discussion.edit', [$discussion->category, $discussion]) }}"
                                       class="ml-2 text-sm text-gray-600">
                                        Edit
                                    </a>
                                @endcan
                                @can ('delete', $discussion)
                                    <form action="{{ route('discussion.delete', [$discussion->category, $discussion]) }}"
                                          method="POST">
                                        @csrf
                                        @method ('DELETE')

                                        <button class="ml-2 text-sm text-gray-600"
                                                onclick="return confirm('Are you sure you want to delete this ' +
                                                 'discussion?')"
                                        >Delete
                                        </button>
                                    </form>
                                @endcan
                            </div>

                            <div class="inline-block mr-2">
                                <a href="#" class="ml-2 text-sm text-gray-600">
                                    Report
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="flex items-start mt-6">
                        <div class="flex-shrink-0">
                            <img
                                    src="{{ $discussion->user->avatar() }}"
                                    alt="{{ $discussion->user->username }}'s avatar"
                                    class="h-12 w-12 rounded-full object-cover"
                            />
                        </div>
                        <div class="ml-6 flex-shrink-1">
                            <div class="flex items-center leading-none">
                                <a
                                        href="#"
                                        class="text-xl text-gray-800 font-bold"
                                >
                                    {{ $discussion->user->username }}
                                </a>
                                <span class="ml-2 text-gray-600">
                                    {{ $discussion->created_at->diffForHumans() }}
                                </span>
                            </div>

                            <div class="mt-6">
                                <p class="text-gray-700 leading-relaxed">
                                    {{ $discussion->body }}
                                </p>
                            </div>

                            <div class="mt-6">
                                <div class="flex items-center text-gray-700">
                                    @if (!$discussion->likers->contains(auth()->user()))
                                        <form action="{{ route('discussion.like.store', [$discussion->category,
                                        $discussion]) }}" method="POST">
                                            @csrf

                                            <button type="submit" class="flex justify-center items-center h-10 w-10
                                            rounded-full
                                            focus:outline-none
                                             focus:shadow-outline bg-transparent">
                                                <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none">
                                                    <path d="M4.31802 6.31802C2.56066 8.07538 2.56066 10.9246 4.31802 12.682L12.0001 20.364L19.682 12.682C21.4393 10.9246 21.4393 8.07538 19.682 6.31802C17.9246 4.56066 15.0754 4.56066 13.318 6.31802L12.0001 7.63609L10.682 6.31802C8.92462 4.56066 6.07538 4.56066 4.31802 6.31802Z"
                                                          stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </button>
                                        </form>
                                    @else
                                        <form action="{{ route('discussion.like.destroy', [$discussion->category,
                                        $discussion]) }}" method="POST">
                                            @csrf
                                            @method ('DELETE')

                                            <button type="submit" class="flex justify-center items-center h-10 w-10
                                            rounded-full focus:outline-none focus:shadow-outline
                                            text-blue-100 bg-blue-500">
                                                <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none">
                                                    <path d="M4.31802 6.31802C2.56066 8.07538 2.56066 10.9246 4.31802 12.682L12.0001 20.364L19.682 12.682C21.4393 10.9246 21.4393 8.07538 19.682 6.31802C17.9246 4.56066 15.0754 4.56066 13.318 6.31802L12.0001 7.63609L10.682 6.31802C8.92462 4.56066 6.07538 4.56066 4.31802 6.31802Z"
                                                          stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>
                                            </button>
                                        </form>
                                    @endif


                                    <span class="ml-2 font-medium">
                                        {{ $discussion->likes->count() }} {{ \Str::plural('like',
                                        $discussion->likes->count()) }}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-16">
                    <div>
                        <h2 class="text-2xl text-gray-800 font-bold">
                            {{ $discussion->replies->count() }} replies
                        </h2>
                    </div>
                    @can ('reply', $discussion)
                        <div class="mt-6">
                            <form action="{{ route('discussion.reply.store', [$discussion->category, $discussion]) }}"
                                  method="POST" class="flex">
                                @csrf

                                <label for="body">
                                    <img
                                            src="{{ auth()->user()->avatar() }}"
                                            alt="{{ auth()->user()->username }}'s avatar"
                                            class="h-12 w-12 rounded-full object-cover"
                                    />
                                </label>

                                <div class="ml-6 flex-1">
                                        <textarea
                                                id="body"
                                                name="body"
                                                placeholder="Join in on the fun and reply to this discussion!"
                                                rows="3"
                                                class="w-full input border-dashed"
                                        ></textarea>

                                    <div class="mt-4">
                                        <button type="submit" class="btn">
                                            Reply to discussion
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endcan
                    <div class="mt-8">
                        @foreach ($replies as $reply)
                            @include ('discussions.partials.reply')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        @include ('discussions.partials.sidebar')
    </div>
@endsection
