@extends('layouts.app')

@section('content')
    @include ('discussions.partials.header', [
        'title' => (isset($currentCategory) && $currentCategory !== null) ? $currentCategory->title : 'Categories',
        'searchEnabled' => true,
    ])

    <div class="mt-8 xl:flex xl:justify-between">
        <div class="w-full xl:w-9/12 xl:pr-12">
            @isset($categories)
                @foreach ($categories as $category)
                    @include ('discussions.partials.category_card')
                @endforeach
            @endisset

            @isset($discussions)
                @foreach ($discussions as $discussion)
                    @include ('discussions.partials.discussion_card')
                @endforeach
            @endisset
        </div>

        @include ('discussions.partials.sidebar')
    </div>
@endsection
