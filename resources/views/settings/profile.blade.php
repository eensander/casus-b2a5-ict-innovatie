@extends('layouts.settings')

@section('content')
    <div class="flex items-center justify-between pb-3  mt-4 mb-2">
        <h2 class="text-3xl text-gray-900 font-bold">Edit your profile</h1>
    </div>
    <div class="flex flex-col">
        <div class="w-full md:w-1/2">
            <div class="bg-white p-8 rounded-md shadow-lg">

				<!-- default form -->
				<form class="w-full max-w-lg md:max-w-full" action="{{ route('settings.profile.update') }}" method="POST">
                    @csrf
                    <input name="_method" type="hidden" value="PATCH">

					<div class="mb-6 px-3 md:flex">
						<label class="block tracking-wide text-gray-700 text-sm font-medium mb-2 md:w-1/4 md:h-full md:text-right md:mr-6 md:py-1" for="grid-username">
							Username
						</label>
                        <div class="md:w-3/4">
						    <input name="username" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-300 rounded py-1 px-3 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 md:text-sm" id="grid-username" type="text" value="{{ Auth::user()->username }}">
                            @error ('username')
                                <p class="text-red-600 text-xs italic">{{ $message }}</p>
                            @enderror
					    </div>
					</div>

					<div class="mb-6 px-3 md:flex">
						<label class="block tracking-wide text-gray-700 text-sm font-medium mb-2 md:w-1/4 md:h-full md:text-right md:mr-6 md:py-1">
							Email
						</label>
                        <div class="md:w-3/4">
                            <input name="asd" class="cursor-not-allowed appearance-none block w-full bg-gray-300 text-gray-700 border border-gray-400 rounded py-1 px-3 mb-3 leading-tight md:text-sm" type="text" value="{{ Auth::user()->email }}" disabled>
						    <p class="text-gray-600 text-xs italic">You can't change your email because you were invited</p>
                        </div>
					</div>

					<div class="mb-6 px-3 md:flex">
						<label class="block tracking-wide text-gray-700 text-sm font-medium mb-2 md:w-1/4 md:h-full md:text-right md:mr-6 md:py-1" for="grid-first-name">
							Gitea-URL
						</label>
                        <div class="md:w-3/4">
                            <input class="cursor-not-allowed appearance-none block w-full bg-gray-300 text-gray-700 border border-gray-400 rounded py-1 px-3 mb-3 leading-tight  md:text-sm" type="text" value="{{ Auth::user()->get_git_url() }}" disabled>
						    <p class="text-gray-600 text-xs italic">You can't change your gitea-url because it was created alongside your account</p>
                        </div>
					</div>

                    {{--
					<div class="mb-6 px-3 md:flex">
						<label class="block tracking-wide text-gray-700 text-sm font-bold mb-2 md:w-1/4 md:h-full md:text-right md:mr-6 md:py-1"  for="grid-homepage">
							Homepage
						</label>
                        <div class="md:w-3/4">
                            <input name="username" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-300 rounded py-1 px-3 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 md:text-sm" type="text" id="grid-homepage" value="{{ Auth::user()->homepage }}">
                        </div>
					</div>

					<div class="mb-6 px-3 md:flex">
						<label class="block tracking-wide text-gray-700 text-sm font-bold mb-2 md:w-1/4 md:h-full md:text-right md:mr-6 md:py-1" for="grid-biography">
							Biography
						</label>
                        <div class="md:w-3/4">
                            <textarea name="biography" class="appearance-none block min-h-10 w-full bg-gray-200 text-gray-700 border border-gray-300 rounded py-1 px-3 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 md:text-sm" id="grid-biography">{{ Auth::user()->biography }}</textarea>
                        </div>
					</div>
                    --}}

                    <div class="mb-2 px-3 md:flex">
                        <div class="md:w-1/4 md:mr-6"></div>
                        <div class="md:w-3/4 ">
                            <button type="submit" class="btn btn-blue">Update</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

    </div>
@endsection
