@extends('layouts.settings')

@section('content')
    <div class="flex items-center justify-between pb-3  mt-4 mb-2">
        <h2 class="text-3xl text-gray-900 font-bold">Edit your password</h1>
    </div>
    <div class="flex flex-col">
        <div class="w-full md:w-1/2">
            <div class="bg-white p-8 rounded-md shadow-lg">

				<!-- default form -->
				<form class="w-full max-w-lg md:max-w-full" action="{{ route('settings.password.update') }}" method="POST">
                    @csrf
                    <input name="_method" type="hidden" value="PATCH">

					<div class="mb-6 px-3 md:flex">
						<label class="block tracking-wide text-gray-700 text-sm font-medium mb-2 md:w-2/5 md:h-full md:text-right md:mr-6 md:py-1" for="field-old-password">
							Old password
						</label>
                        <div class="md:w-3/5">
						    <input name="old_password" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-300 rounded py-1 px-3 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 md:text-sm" id="field-old-password" type="password" placeholder="**********">
                            @error ('old_password')
                                <p class="text-red-600 text-xs italic">{{ $message }}</p>
                            @enderror
					    </div>
					</div>

                    <div class="border-b border-gray-300 mb-6"></div>

					<div class="mb-6 px-3 md:flex">
						<label class="block tracking-wide text-gray-700 text-sm font-medium mb-2 md:w-2/5 md:h-full md:text-right md:mr-6 md:py-1" for="field-new-password">
							New password
						</label>
                        <div class="md:w-3/5">
						    <input name="new_password" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-300 rounded py-1 px-3 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 md:text-sm" id="field-new-password" type="password" placeholder="**********">
                            @error ('new_password')
                                <p class="text-red-600 text-xs italic">{{ $message }}</p>
                            @enderror
					    </div>
					</div>
					<div class="mb-6 px-3 md:flex">
						<label class="block tracking-wide text-gray-700 text-sm font-medium mb-2 md:w-2/5 md:h-full md:text-right md:mr-6 md:py-1" for="field-new-password-confirm">
							Confirm new password
						</label>
                        <div class="md:w-3/5">
						    <input name="new_password_confirmation" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-300 rounded py-1 px-3 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 md:text-sm" id="field-new-password-confirm" type="password" placeholder="**********">
					    </div>
					</div>

                    <div class="mb-2 px-3 md:flex">
                        <div class="md:w-1/4 md:mr-6"></div>
                        <div class="md:w-3/4">
                            <button type="submit" class="btn btn-blue">Update</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

    </div>
@endsection
