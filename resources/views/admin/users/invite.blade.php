@extends('layouts.app')

@section('content')


<h1 class="text-gray-700 tracking-wider mx-1">
    Importeer
</h1>

<div class="relative mb-8">
    <div class="bg-white rounded-t-lg border border-gray-400 p-4 mt-4 max-w-2xl">

        @isset($message)
        <p class="mb-4">{{ $message }}</p>
        @endisset

        @if(!is_null(session('amount_success')) && session('amount_success') > 0)
            <div class="bg-green-100 border-t-4 border-green-500 rounded-b text-green-900 px-4 py-3 shadow-md mb-6" role="alert">
                <div class="flex">
                    <div class="py-1"><svg class="fill-current h-6 w-6 text-green-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                    <div>
                        <p class="font-bold">{{ session('amount_success') }} {{ Str::plural('email', session('amount_success')) }} have been sent.</p>
                    </div>
                </div>
            </div>
        @endisset

        @if(is_array(session('email_errors')) && !empty(session('email_errors')))
            <div class="bg-red-100 border-t-4 border-red-500 rounded-b text-red-900 px-4 py-3 shadow-md mb-6" role="alert">
                <div class="flex">
                    <div class="py-1"><svg class="fill-current h-6 w-6 text-red-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                    <div>
                        <p class="font-bold">Not all emails have been sent successfully.</p>
                        @if(!is_null(session('amount_success')) && !is_null(session('amount_total') ))
                            <p class="text-sm">{{ session('amount_failed') }} of the {{ session('amount_total') }} ({{ sprintf("%01.2f", (session('amount_failed')/session('amount_total'))*100) }}%) of the e-mails have failed to send.</p>
                        @endif

                        @if(is_array(session('email_errors')))
                            <ul class="list-disc">
                            @foreach(session('email_errors') as $email_error)
                                <li class="text-red-500">{{ $email_error['email'] }}, {{ $email_error['error'] }}</li>
                            @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        @endif

        <form class="w-full max-full" method="POST" action="{{ route('admin.invite-users.send') }}">
        @csrf

            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                        The invite-links expire:
                    </label>
                    <label><input name="expiration_enabled" class="bg-gray-200 text-gray-700 border border-red-500 mb-3 focus:outline-none focus:bg-white" type="radio" value="1" {{ is_null(old('expiration_enabled')) || old('expiration_enabled') == '1'  ? 'checked' :'' }} > Yes</label>
                    <label><input name="expiration_enabled" class="bg-gray-200 text-gray-700 border border-red-500 mb-3 focus:outline-none focus:bg-white" type="radio" value="0" {{ old('expiration_enabled') == '0' ? 'checked' :'' }} > No</label>
                    @error('expiration_enabled')
                        <p class="text-red-600 text-xs italic">{{ $message }}</p>
                    @enderror
                </div>
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                        Expiration date
                    </label>
                    <input name="expiration_date" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-grey-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="date" placeholder="Jane" value="{{ old('expiration_date') }}">
                    @error('expiration_date')
                        <p class="text-red-600 text-xs italic">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold" for="grid-password">
                        emailaddresses
                    </label>
                    <p class="text-gray-700 text-xs italic mb-3">one per line</p>
                    <textarea name="emailaddresses" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">{{old('emailaddresses')}}</textarea>
                    @error('emailaddresses')
                        <p class="text-red-600 text-xs italic">{{ $message }}</p>
                    @enderror
                    {{-- <p class="text-gray-600 text-xs italic">Make it as long and as crazy as you'd like</p> --}}
                </div>
            </div>
            <div class="flex flex-wrap flex-row-reverse -mx-3 mb-2">
                <div class="w-full md:w-1/3 mb-0 px-3">
                    <button type="submit" class="w-full btn btn-blue">
                    Send
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>


@endsection


@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
