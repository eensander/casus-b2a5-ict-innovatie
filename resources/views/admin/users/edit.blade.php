@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3 text-xl mb-2">Update a user</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('admin.users.update', $user) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="first_name" class="w-32 inline-block">Name:</label>
                <input type="text" class="form-control mb-2 pl-1 border" name="username" value={{ $user->username }} >
            </div>
            <div class="form-group">
                <label for="email" class="w-32 inline-block">Email:</label>
                <input type="text" class="form-control mb-2 pl-1 border" name="email" value={{ $user->email }} >
            </div>
            <div class="form-group">
                <label for="city" class="w-32 inline-block">Password:</label>
                <input type="text" class="form-control mb-2 pl-1 border" name="password" value={{ $user->password }} >
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
