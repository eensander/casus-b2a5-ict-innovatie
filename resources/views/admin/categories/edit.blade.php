@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3 text-xl mb-1">Update a category</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('admin.categories.update', $category) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="first_name" class="w-32 inline-block">parentid:</label>
                <input type="text" class="form-control mb-2 pl-1 border" name="parent_id" value={{ $category->parent_id }} >
            </div>
            <div class="form-group">
                <label for="first_name" class="w-32 inline-block">Slug:</label>
                <input type="text" class="form-control mb-2 pl-1 border" name="slug" value={{ $category->slug }} >
            </div>
            <div class="form-group">
                <label for="email" class="w-32 inline-block">Title:</label>
                <input type="text" class="form-control mb-2 pl-1 border" name="title" value={{ $category->title }} >
            </div>
            <div class="form-group">
                <label for="email" class="w-32 inline-block">Order:</label>
                <input type="text" class="form-control mb-2 pl-1 border" name="order" value={{ $category->order }} >
            </div>
            <div class="form-group">
                <label for="city" class="w-32 inline-block">Description:</label>
                <textarea class="form-control mb-2 pl-1 border max-w-xs w-full" name="body">{{ $category->body }}</textarea>
            </div>
            <div class="form-group">
                <label for="city" class="w-32 inline-block">Image URL:</label>
                <input type="text" class="form-control mb-2 pl-1 border" name="image_url" value={{ $category->image_url }} >
            </div>
            <div class="form-group">
                <label for="city" class="w-32 inline-block">Closed:</label>
                <input type="text" class="form-control mb-2 pl-1 border" name="is_closed" value={{ $category->is_closed }} >
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
