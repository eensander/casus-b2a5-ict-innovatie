const {screens} = require('tailwindcss/defaultTheme');
const {colors} = require('tailwindcss/defaultTheme');
const {fontFamily} = require('tailwindcss/defaultTheme');

module.exports = {
    theme: {
        screens: {
            'sm': screens.sm,
            'md': screens.md,
            'lg': screens.lg,
            'xl': screens.xl,
            '2xl': '1600px',
        },
        colors: {
            transparent: colors.transparent,
            white: colors.white,
            black: colors.black,
            gray: colors.gray,
            blue: colors.blue,
            green: {
                '100': '#F0FFF4',
                '200': '#C6F6D5',
                '300': '#9AE6B4',
                '400': '#48BB78',
                '500': '#38A169',
                '600': '#2F855A',
                '700': '#22543D',
            },
            yellow: {
                '100': '#FFFFF0',
                '200': '#FEFCBF',
                '300': '#FAF089',
                '400': '#ECC94B',
                '500': '#D69E2E',
                '600': '#B7791F',
                '700': '#744210',
            },
            red: {
                '100': '#FFF5F5',
                '200': '#FEB2B2',
                '300': '#FC8181',
                '400': '#E53E3E',
                '500': '#C53030',
                '600': '#9B2C2C',
                '700': '#742A2A',
            },
        },
        extend: {
            fontFamily: {
                'sans': [
                    'Inter',
                    fontFamily.sans,
                ],
            },
        },
    },
    variants: {},
    plugins: [],
}
