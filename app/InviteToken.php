<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;
use \Carbon\Carbon as Carbon;

class InviteToken extends Model
{

    protected $primaryKey = 'uuid';

    protected $keyType = 'string';

    public $incrementing = false;

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }

    public function is_expired(): bool
    {
        return !(is_null($this->expires_at) || Carbon::parse($this->expires_at)->isFuture());
    }

    public function validate()
    {
        $login_enabled = false;

        if ($this->is_expired())
        {
            $message = 'This invite-link has expired. Please request a new one';
        }
        elseif ($this->used == true)
        {
            $message = 'This invite-link has already been used.';
        }
        else
        {
            $login_enabled = true;
            $message = 'You have been invited to our platform, please fill out the form below to create an account.';
        }

        return [$login_enabled, $message];
    }

    public function invite_link()
    {
        return route('register') . "?t=" . $this->uuid;
    }

}
