<?php

namespace App\Policies;

use App\Category;
use App\Discussion;
use App\Reply;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DiscussionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create discussions.
     *
     * @param User     $user
     * @param Category $category
     *
     * @return mixed
     */
    public function create(User $user, Category $category)
    {
        return !$category->is_closed || $user->hasPermissionTo('discussion.create');
    }

    /**
     * Determine whether the user can update the discussion.
     *
     * @param User            $user
     * @param \App\Discussion $discussion
     *
     * @return mixed
     */
    public function update(User $user, Discussion $discussion)
    {
        return $user->id === $discussion->user->id || $user->hasPermissionTo('discussion.update');
    }

    /**
     * Determine whether the user can delete the discussion.
     *
     * @param User            $user
     * @param \App\Discussion $discussion
     *
     * @return mixed
     */
    public function delete(User $user, Discussion $discussion)
    {
        return $user->id === $discussion->user->id || $user->hasPermissionTo('discussion.delete');
    }

    /**
     * Determine whether the user can reply to the discussion.
     *
     * @param User       $user
     * @param Discussion $discussion
     * @param Reply|null $reply
     *
     * @return bool
     */
    public function reply(User $user, Discussion $discussion, Reply $reply = null)
    {
        if ($discussion->is_locked) {
            if (!$user->hasPermissionTo('discussion.reply'))
                return false;
        }

        if ($reply !== null) {
            if ($reply->parentReply !== null) {
                return false;
            }
        }

        return true;
    }

    public function bestReply(User $user, Discussion $discussion, Reply $reply)
    {
        if ($discussion->user->id !== $user->id) {
            return false;
        }

        if ($reply->parentReply !== null) {
            return false;
        }

        if ($reply->discussion === $discussion->id) {
            return false;
        }

        return true;
    }
}
