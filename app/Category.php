<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Sluggable;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function path()
    {
        return route('discussion.index', $this);
    }

    public function discussions()
    {
        return $this->hasMany('App\Discussion');
    }

    public function latestDiscussion()
    {
        return $this->hasOne(Discussion::class)->latest();
    }

    public function replies()
    {
        return $this->hasManyThrough(Reply::class, Discussion::class);
    }

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id')->with('children');
    }

    public function breadcrumbs()
    {
        $breadcrumbs = [];

        $breadcrumbs = array_merge($breadcrumbs, [
            $this->title => [
                'title' => $this->title,
                'url' => $this->path(),
            ],
        ]);

        $parentCategory = $this->parent;
        while ($parentCategory != null) {
            $breadcrumbs = array_merge($breadcrumbs, [
                $parentCategory->title => [
                    'title' => $parentCategory->title,
                    'url' => $parentCategory->path(),
                ],
            ]);

            $parentCategory = $parentCategory->parent;
        }

        $breadcrumbs = array_reverse($breadcrumbs);

        return $breadcrumbs;
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }
}
