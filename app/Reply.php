<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $fillable = [
        'parent_id',
        'user_id',
        'body',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function discussion()
    {
        return $this->belongsTo(Discussion::class);
    }

    public function bestReplyOf()
    {
        return $this->belongsTo('App\Discussion');
    }

    public function isBestReply()
    {
        return $this->discussion->best_reply_id === $this->id;
    }

    public function likes()
    {
        return $this->morphMany('App\Like', 'likeable');
    }

    public function likers()
    {
        return $this->hasManyThrough(User::class, Like::class, 'likeable_id', 'id', 'id', 'user_id')->where('likeable_type', Reply::class);
    }

    public function parentReply()
    {
        return $this->belongsTo(Reply::class, 'parent_id', 'id');
    }

    public function replies()
    {
        return $this->hasMany(Reply::class, 'parent_id', 'id');
    }
}
