<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    use Sluggable;

    protected $fillable = [
        'category_id',
        'best_reply_id',
        'title',
        'body',
        'is_locked',
        'is_pinned',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function path()
    {
        return route('discussion.show', [
            $this->category,
            $this,
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function replies()
    {
        return $this->hasMany(Reply::class)->where('parent_id', null);
    }

    public function bestReply()
    {
        return $this->hasOne(Reply::class, 'id', 'best_reply_id');
    }

    public function latestReply()
    {
        return $this->hasOne(Reply::class)->latest();
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function likers()
    {
        return $this->hasManyThrough(User::class, Like::class, 'likeable_id', 'id', 'id', 'user_id')->where('likeable_type', Discussion::class);
    }

    public function breadcrumbs()
    {
        $breadcrumbs = $this->category->breadcrumbs();

        $breadcrumbs = array_merge($breadcrumbs, [
            $this->title => [
                'title' => $this->title,
                'url' => $this->path(),
            ],
        ]);

        return $breadcrumbs;
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }
}
