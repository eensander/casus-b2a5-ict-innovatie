<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    protected $fillable = ['message'];

    public function source_user()
    {
        return $this->belongsTo('App\User', 'source_user_id');
    }

    public function destination_user()
    {
        return $this->belongsTo('App\User', 'destination_user_id');
    }

    /**
     * Retrieve public data as querybuilder scope
     */
    /*
    public function scopePublicData($q)
    {
        return $q
            ->select('id', 'content', 'source_user_id', 'destination_user_id', 'created_at', 'updated_at')
            ->with(['source_user' => function($query) {
                $query->select('id', 'username');
            }])
            ->with(['destination_user' => function($query) {
                $query->select('id', 'username');
            }]);
    }
    */

    /**
     * Retrieve public data as array, trying to replicate scopePublicData as much as possible
     */
    public function getPublicData()
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'source_user' => $this->source_user->getPublicData(),
            'destination_user' => $this->destination_user->getPublicData(),
            'created_at' => $this->created_at->toDateTimeString(),
            'created_ago' => $this->created_at->diffForHumans(['short' => true ]),
            // 'created_ago' => $this->created_at->diffInSeconds(),
            'is_sent' => (Auth::user()->id == $this->source_user->id) // if false: this message is received
        ];
    }

}
