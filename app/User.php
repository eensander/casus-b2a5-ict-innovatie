<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Traits\HasPermissions;

class User extends Authenticatable {
    use Notifiable;
    use HasRoles;

    private $_gitea_client = null;
    private $_gitea_user   = null;

    public function getRouteKeyName()
    {
        return 'username';
    }

    /**
     * Messages directed to this user
     */
    public function inbox_messages()
    {
        return $this->hasMany('App\Message', 'destination_user_id')->orderBy('created_at', 'DESC');
    }

    /**
     * Messages sent by this user
     */
    public function outbox_messages()
    {
        return $this->hasMany('App\Message', 'source_user_id');
    }

    public function avatar()
    {
        return 'https://www.gravatar.com/avatar/' . md5(strtolower(trim(auth()->user()->email))) . '?d=mp';
    }


    public function projects()
    {
        return $this->belongsToMany('App\Project', 'project_user');
    }

    public function get_git_url()
    {
        if ($this->gitea_id === null)
        {
            return 'no-associated-account';
        }

        return env('GITEA_URL') . '/' . $this->username;
        // return $this->_gitea_user;
    }

    /**
     * Retrieve public data as array
     */
    public function getPublicData()
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'avatar' => $this->avatar(),
            'git_url' => $this->get_git_url()
        ];
    }

    public function discussions()
    {
        return $this->hasMany(Discussion::class);
    }


    /**
     * Items that the user has liked, NOT the likes received by this user
     */
    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
