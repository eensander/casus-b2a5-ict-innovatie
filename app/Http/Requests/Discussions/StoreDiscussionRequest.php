<?php

namespace App\Http\Requests\Discussions;

use App\Category;
use App\Discussion;
use Illuminate\Foundation\Http\FormRequest;

class StoreDiscussionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Category $category)
    {
        return $this->user()->can('create', [
            Discussion::class,
            $category,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:4|max:50',
            'body' => 'required|min:4',
        ];
    }
}
