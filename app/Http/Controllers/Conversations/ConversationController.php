<?php

namespace App\Http\Controllers\Conversations;

use App\User;
use App\Message;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Events\MessageSent;

use Illuminate\Support\Facades\Auth;

class ConversationController extends Controller
{
    /**
     * Show the main messages-view
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('conversations.index');
    }

    /**
     * Show the main messages-view
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function new(Request $request)
    {
        if (!isset($request["username"]))
            return ['status' => false, 'message' => 'Could not process request'];

        $username = trim($request["username"]);

        if (trim($username) == "")
        {
            return ['status' => false, 'message' => 'Username can\'t be empty'];
        }

        $user = User::where('username', $username)->first();
        if (is_null($user))
        {
            return ['status' => false, 'message' => 'User not found'];
        }

        return ['status' => true, 'partner' => $user->getPublicData(), 'message' => null];

    }

    /**
     * Show the main messages with exchanged with another user
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function messages(User $user)
    {
        $src = $user->id; // source
        $dst = Auth::user()->id; // destination

        // (dest = 1 AND source = 3) OR (dest = 3 AND source = 1)

        return ['messages' => Message::where(function($query) use ($src, $dst) {
                $query->where('destination_user_id', $src)
                      ->Where('source_user_id', $dst);
            })
            ->orWhere(function($query) use ($src, $dst) {
                $query->where('destination_user_id', $dst) // id's switched arround
                      ->Where('source_user_id', $src);
            })
            ->orderBy('created_at', 'ASC')
            ->get()->map(function($x) {
                return $x->getPublicData();
            })];
    }

    /**
     * Retrieve user's conversations
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function get_conversations()
    {
        // return Auth::user()->inbox_messages->unique('source_user_id')->map(function($x) {
        //     return $x->getPublicData();
        // })->reverse();

        $user_id = Auth::user()->id;

        return Message::where('destination_user_id', $user_id)
            ->orWhere('source_user_id', $user_id)
            ->orderBy('created_at', 'DESC')
            ->get()
            ->unique(function($x) use ($user_id) {
                // to make unique per pair (or tuple) of conversation partners, where always:
                // authorized user comes first, partner comes second

                if ($x->source_user_id == $user_id) // it's me
                {
                    return $x->source_user_id .'-'. $x->destination_user_id;
                }
                else
                {
                    return $x->destination_user_id .'-'. $x->source_user_id;
                }
            })
            ->map(function($x) use ($user_id) {

                if ($x->source_user_id == $user_id) // it's me
                {
                    $partner = $x->destination_user;
                }
                else
                {
                    $partner = $x->source_user;
                }

                return ['message' => $x->getPublicData(), 'partner' => $partner->getPublicData()];
            });
    }

    /**
    * Show the main messages-view
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function send(User $user, $source_user = null)
    {
        $message = new Message;

        $message->content = request()->json()->get('content');
        $message->source_user_id = $source_user ?? Auth::user()->id;
        $message->destination_user_id = $user->id;
        $message->save();

        $public_message = $message->getPublicData();

        // Announce that a new message has been posted
        // broadcast(new MessageSent($public_message));

        return [
            'status' => 'OK',
            'message' => $public_message
        ];
    }
}
