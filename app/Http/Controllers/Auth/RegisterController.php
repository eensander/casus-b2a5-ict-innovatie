<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use Illuminate\Auth\Events\Registered;

use App\InviteToken;
use Avency\Gitea;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:40', 'unique:users', 'alpha_dash'], // alpha_dash for gitea
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * Initialize gitea connectio to $this->giteaClient, or return register page with error
     * @return  null | view     Null on succes, view on failure (with parameters)
     */
    private function initGiteaConnection()
    {
        try {
            $this->giteaClient = new Gitea\Client(
                env('GITEA_URL'),
                [
                    'type' => Gitea\Client::AUTH_TOKEN,
                    'auth' => env('GITEA_ACCESS_TOKEN')
                ]
            );
            $this->giteaClient->Miscellaneous()->version(); // to throw error when gitea is not available]
            return null;
        }
        catch (\Exception $e)
        {
            return view('auth.register', [
                'register_enabled' => false,
                'header_message' => 'Registration is not available at this moment. Could not connect to gitea server. Please try again later'
            ]);
        }
    }


    /**
     * OVERIDE Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        // disable registration if gitea is not running
        $gitea_connection = $this->initGiteaConnection();
        if (!is_null($gitea_connection))
            return $gitea_connection;

        $register_enabled = false;
        $header_message = 'An unexpected error occured. Please try again later.';
        $invite = null;

        if (!isset($_GET['t']) || empty($_GET['t']))
        {
            $header_message = 'Registering is currently only available on invite-basis.';
        }
        elseif (isset($_GET['t']) && !empty($_GET['t']))
        {

            $invite = InviteToken::find($_GET['t']);

            if (is_null($invite))
            {
                $header_message = 'The invite-link you provided is not valid.';
            }
            else
            {
                list($register_enabled, $header_message) = $invite->validate();
            }

        }

        return view('auth.register', compact('header_message', 'register_enabled', 'invite'));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // disable registration if gitea is not running
        $gitea_connection = $this->initGiteaConnection();
        if (!is_null($gitea_connection))
            return $gitea_connection;

        $invite = InviteToken::find($request['invite_token']);

        if (is_null($invite))
        {
            return back(); // re-renders message
        }
        else
        {
            list($register_enabled, $message) = $invite->validate();
            if (!$register_enabled)
            {
                return back(); // re-renders message
            }
            else
            {
                $request['email'] = $invite->email;
            }
        }

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    protected function registered(Request $request, User $user)
    {
        $invite = InviteToken::find($request['invite_token']);
        $invite->used = true;

        $invite->save();

        $giteaUser = $this->giteaClient->admin()->createUser(
            $user->username,
            $request['password'], // will be added on account creation.
            $user->email,
            null, // full name
            null, // loginname (?)
            false, // mustChangePassword
            true // sendNotify
        );

        $user['gitea_id'] = $giteaUser['id'];
        $user->save();

    }

}
