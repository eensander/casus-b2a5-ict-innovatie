<?php

namespace App\Http\Controllers\Discussions\Replies;

use App\Category;
use App\Discussion;
use App\Http\Controllers\Controller;
use App\Reply;
use Illuminate\Http\Request;

class DiscussionBestReplyController extends Controller
{
    public function store(Category $category, Discussion $discussion, Reply $reply, Request $request)
    {
        $this->authorize('bestReply', [
            $discussion,
            $reply,
        ]);

        $discussion->update([
            'best_reply_id' => $reply->id,
        ]);

        return back()->with([
            'status' => 'Successfully made this the best reply',
            'type' => 'success',
            'duration' => 10,
        ]);
    }
}
