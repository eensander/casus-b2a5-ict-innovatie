<?php

namespace App\Http\Controllers\Discussions\Replies;

use App\Category;
use App\Discussion;
use App\Http\Controllers\Controller;
use App\Reply;
use Illuminate\Http\Request;

class DiscussionReplyLikeController extends Controller
{
    /**
     * @param Category   $category
     * @param Discussion $discussion
     * @param Reply|null $reply
     * @param Request    $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Category $category, Discussion $discussion, Reply $reply, Request $request)
    {
        if ($reply->likers->contains($request->user())) {
            return response(null, 409);
        }

        $reply->likes()->create([
            'user_id' => $request->user()->id,
        ]);

        return back()->with([
            'status' => 'Successfully liked the reply',
            'type' => 'success',
            'duration' => 10,
        ]);
    }

    public function delete(Category $category, Discussion $discussion, Reply $reply, Request $request)
    {
        if (!$reply->likers->contains($request->user())) {
            return response(null, 409);
        }

        $like = $reply->likes->where('user_id', $request->user()->id);

        $like->each->delete();

        return back()->with([
            'status' => 'Successfully unliked the reply',
            'type' => 'success',
            'duration' => 10,
        ]);
    }
}
