<?php

namespace App\Http\Controllers\Discussions\Replies;

use App\Category;
use App\Discussion;
use App\Http\Controllers\Controller;
use App\Reply;
use Illuminate\Http\Request;

class DiscussionReplyController extends Controller
{
    public function store(Category $category, Discussion $discussion, Request $request, Reply $reply = null)
    {
        $this->authorize('reply', [
            $discussion,
            $reply,
        ]);

        $request->validate([
            'body' => 'required|min:3',
        ]);

        if ($reply === null) {
            $discussion->replies()->create([
                'user_id' => $request->user()->id,
                'body' => $request->body,
            ]);
        }
        else {
            $discussion->replies()->create([
                'parent_id' => $reply->id,
                'user_id' => $request->user()->id,
                'body' => $request->body,
            ]);
        }

        return back()->with([
            'status' => 'Successfully replied to the discussion',
            'type' => 'success',
            'duration' => 10,
        ]);
    }
}
