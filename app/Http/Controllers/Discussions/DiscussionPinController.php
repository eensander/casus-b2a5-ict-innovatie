<?php

namespace App\Http\Controllers\Discussions;

use App\Category;
use App\Discussion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class DiscussionPinController extends Controller
{
    public function store(Category $category, Discussion $discussion)
    {
        if (!Auth::user()->hasPermissionTo('discussion.pin'))
        {
            return abort(403, "This action is unauthorized.");
        }

        $discussion->update([
            'is_pinned' => true,
        ]);

        return back()->with([
            'status' => 'Successfully pinned this discussion',
            'type' => 'success',
            'duration' => 10,
        ]);
    }

    public function destroy(Category $category, Discussion $discussion)
    {
        if (!Auth::user()->hasPermissionTo('discussion.pin'))
        {
            return abort(403, "This action is unauthorized.");
        }

        $discussion->update([
            'is_pinned' => false,
        ]);

        return back()->with([
            'status' => 'Successfully unpinned this discussion',
            'type' => 'success',
            'duration' => 10,
        ]);
    }
}
