<?php

namespace App\Http\Controllers\Discussions;

use App\Category;
use App\Discussion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class DiscussionLockController extends Controller
{
    public function store(Category $category, Discussion $discussion)
    {
        if (!Auth::user()->hasPermissionTo('discussion.lock'))
        {
            return abort(403, "This action is unauthorized.");
        }

        $discussion->update([
            'is_locked' => true,
        ]);

        return back()->with([
            'status' => 'Successfully locked this discussion',
            'type' => 'success',
            'duration' => 10,
        ]);
    }

    public function destroy(Category $category, Discussion $discussion)
    {
        if (!Auth::user()->hasPermissionTo('discussion.lock'))
        {
            return abort(403, "This action is unauthorized.");
        }

        $discussion->update([
            'is_locked' => false,
        ]);

        return back()->with([
            'status' => 'Successfully unlocked this discussion',
            'type' => 'success',
            'duration' => 10,
        ]);
    }
}
