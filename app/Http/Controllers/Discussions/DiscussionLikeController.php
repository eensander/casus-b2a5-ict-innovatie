<?php

namespace App\Http\Controllers\Discussions;

use App\Category;
use App\Discussion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DiscussionLikeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function store(Category $category, Discussion $discussion, Request $request)
    {
        if ($discussion->likers->contains($request->user())) {
            return response(null, 409);
        }

        $discussion->likes()->create([
            'user_id' => $request->user()->id,
        ]);

        return back()->with([
            'status' => 'Successfully liked the discussion',
            'type' => 'success',
            'duration' => 10,
        ]);
    }

    public function destroy(Category $category, Discussion $discussion, Request $request)
    {
        if (!$discussion->likers->contains($request->user())) {
            return response(null, 409);
        }

        $like = $discussion->likes->where('user_id', $request->user()->id);

        $like->each->delete();

        return back()->with([
            'status' => 'Successfully unliked the discussion',
            'type' => 'success',
            'duration' => 10,
        ]);
    }
}
