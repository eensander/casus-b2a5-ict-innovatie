<?php

namespace App\Http\Controllers\Discussions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Discussions\StoreDiscussionRequest;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use App\Discussion;
use App\Category;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class DiscussionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Category|null $category
     *
     * @return View
     */
    public function index(Category $category = null)
    {
        $categories = $this->getCategories($category);
        $discussions = $this->getDiscussions($category);

        return view('discussions.index', [
            'breadcrumbs' => optional($category)->breadcrumbs(),
            'currentCategory' => $category,
            'categories' => $categories,
            'discussions' => $discussions,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Category $category
     *
     * @return View
     */
    public function create(Category $category)
    {
        $this->authorize('create', [
            Discussion::class,
            $category,
        ]);

        return view('discussions.create', [
            'breadcrumbs' => $category->breadcrumbs(),
            'currentCategory' => $category,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Category $category
     * @param Request  $request
     *
     * @return RedirectResponse|Redirector
     */
    public function store(StoreDiscussionRequest $request, Category $category)
    {
        $discussion = $request->user()->discussions()->create([
            'category_id' => $category->id,
            'title' => $request->title,
            'body' => $request->body,
        ]);

        return redirect(route('discussion.show', [
            $category,
            $discussion,
        ]))->with([
            'status' => 'Successfully published your discussion',
            'type' => 'success',
            'duration' => 10,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Category   $category
     * @param Discussion $discussion
     *
     * @return Factory|View
     */
    public function show(Category $category, Discussion $discussion, Request $request)
    {
        return view('discussions.show', [
            'breadcrumbs' => $discussion->breadcrumbs(),
            'discussion' => $discussion,
            'replies' => $discussion->replies->where('parent_id', null),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category   $category
     * @param Discussion $discussion
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function edit(Category $category, Discussion $discussion)
    {
        $this->authorize('update', [
            $discussion,
        ]);

        return view('discussions.edit', [
            'breadcrumbs' => $discussion->breadcrumbs(),
            'discussion' => $discussion,
            'replies' => $discussion->replies->where('parent_id', null),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request    $request
     * @param Category   $category
     * @param Discussion $discussion
     *
     * @return RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function update(Request $request, Category $category, Discussion $discussion)
    {
        $this->authorize('update', [
            $discussion,
        ]);

        $request->validate([
            'title' => 'required|min:4|max:50',
            'body' => 'required|min:4',
        ]);

        $discussion->update([
            'title' => $request->title,
            'body' => $request->body,
        ]);

        return redirect(route('discussion.show', [
            $category,
            $discussion,
        ]))->with([
            'status' => 'Successfully edited your discussion',
            'type' => 'success',
            'duration' => 10,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category   $category
     * @param Discussion $discussion
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy(Category $category, Discussion $discussion)
    {
        $this->authorize('delete', [
            $discussion,
        ]);

        $discussion->delete();

        return redirect(route('discussion.index', [
            $category,
        ]))->with([
            'status' => 'Successfully deleted your discussion',
            'type' => 'success',
            'duration' => 10,
        ]);
    }

    public function getCategories(Category $category = null)
    {
        if ($category === null) {
            return Category::where('parent_id', null)->orderBy('order')->get();
        }

        return Category::where('parent_id', $category->id)->orderBy('order')->get();
    }

    protected function getDiscussions(Category $category = null)
    {
        if ($category === null) {
            return null;
        }

        return Discussion::where('category_id', $category->id)->latest('is_pinned')->latest()->paginate(20);
    }
}
