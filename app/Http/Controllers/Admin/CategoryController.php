<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id'=>'required',
            'slug'=>'required',
            'title'=>'required',
            'order'=>'required',
            'description'=>'required',
            'img_url'=>'required',
            'is_closed'=>'required'
        ]);

        $cateogry = new Category([
            'id' => $request->get('id'),
            'slug' => $request->get('slug'),
            'title' => $request->get('title'),
            'order' => $request->get('order'),
            'description' => $request->get('description'),
            'img_url' => $request->get('img_url'),
            'is_closed' => $request->get('is_closed')
        ]);
        $category->save();
        return redirect(route('admin.categories.index'))->with('success', 'Category saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //$category = Category::find($id);
        return view('admin.categories.edit', compact('categories', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'slug'=>'required',
            'title'=>'required',
            'order'=>'required',
            'body'=>'required',
            'image_url'=>'required',
            'is_closed'=>'required'
        ]);

        $category->slug = $request->get('slug');
        $category->title = $request->get('title');
        $category->order = $request->get('order');
        $category->body = $request->get('body');
        $category->image_url = $request->get('image_url');
        $category->is_closed = $request->get('is_closed');
        $category->save();

        return redirect(route('admin.categories.index'))->with(['status' => 'Category updated.', 'type' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return redirect(route('admin.categories.index'))->with(['status' => 'Category deleted.', 'type' => 'success']);
    }
}
