<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Mail\Mailer;
use App\InviteToken;

class UserInviteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form()
    {
        return view('admin.users.invite');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function send_invites(Request $request)
    {
        // dd($request);

        $request_valid = $request->validate([
            'expiration_enabled' => ['required', 'boolean'],
            'expiration_date' => ['required_if:expiration_enabled,1', 'nullable', 'date'],
            'emailaddresses' => ['required']
        ],
        [
            'expiration_date.required_if' => 'The expiration date field is required when the invite-links are set to expire.'
        ]);

        $expires_at = null;
        if ($request_valid['expiration_enabled'])
        {
            $expires_at = $request_valid['expiration_date'];
        }

        // https://stackoverflow.com/a/11165332
        $emailaddresses = preg_split("/\r\n|\n|\r/", $request_valid['emailaddresses']);

        $email_errors = [];
        $emails_sent = 0;

        foreach($emailaddresses as $emailaddress)
        {

            if (InviteToken::where('email', $emailaddress)->first() !== null)
            {
                $email_errors[] = [
                    'email' => $emailaddress,
                    'error' => 'Email has already been invited'
                ];
                continue;
            }

            $invite = new InviteToken;
            $invite->email = $emailaddress;
            $invite->expires_at = $expires_at;
            $invite->save();

            try
            {
                Mail::to($emailaddress)->send(new Mailer("mail.invite", ["invite_link" => $invite->invite_link()], "You have been invited to join the Zuyd LoRa Community"));
            }
            catch (\Exception $e)
            {
                $email_errors[] = [
                    'email' => $emailaddress,
                    'error' => $e->getMessage()
                ];
            }

        }

        if (!empty(Mail::failures()))
        {
            foreach(Mail::failures() as $email)
            {
                $email_errors[] = [
                    'email' => $email,
                    'error' => 'was not sent correctly'
                ];
            }
        }

        // calculating amount that succeeded.
        $amount_success = count(array_diff($emailaddresses, array_column($email_errors, 'email')));
        $amount_total = count($emailaddresses);
        $amount_failed = $amount_total - $amount_success;

        return back()->with(compact('email_errors', 'amount_success', 'amount_failed', 'amount_total'));

    }

}
