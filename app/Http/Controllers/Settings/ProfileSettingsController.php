<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ProfileSettingsController extends Controller
{

    /**
     * Show a profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit()
    {
        return view('settings.profile');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {
        $user = Auth::user();

        $valid_data = Validator::make($request->all(), [
            'username' => ['required', 'string', 'max:255', 'unique:users,username,'.$user->id.',id', 'alpha_dash']
        ])->validate();

        $user->fill($valid_data);

        if ($user->isDirty())
            $status = 'Successfully saved';
        else
            $status = 'No changes were made';

        $user->save();

        return redirect()->back()->with([
            'status' => $status,
            'type' => 'success',
            'duration' => 10,
        ]);

    }

}
