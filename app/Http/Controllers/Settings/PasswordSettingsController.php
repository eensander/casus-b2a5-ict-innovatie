<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PasswordSettingsController extends Controller
{

    /**
     * Show a profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit()
    {
        return view('settings.password');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {
        $user = Auth::user();

        $valid_data = Validator::make($request->all(), [
            'old_password' => ['required', 'password:web'],
            'new_password' => ['required', 'string', 'min:8', 'confirmed']
        ])->validate();

        if ($valid_data['old_password'] != $valid_data['new_password'])
        {
            $user->password = Hash::make($valid_data['new_password']);
            $user->save();

            $status = 'Successfully changed your password';
            $type = 'success';
        }
        else
        {
            $status = 'Your password has not been modified';
            $type = 'info';
        }

        return redirect()->back()->with([
            'status' => $status,
            'type' => $type,
            'duration' => 10,
        ]);

    }

}
