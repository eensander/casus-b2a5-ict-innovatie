<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\User;

class ProfileController extends Controller
{

    /**
     * Show a profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(User $user)
    {
        $own_profile = (Auth::id() == $user->id);

        return view('profile.show', compact('user', 'own_profile'));
    }

}
