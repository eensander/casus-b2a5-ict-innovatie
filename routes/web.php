<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::prefix('/auth')->group(function () {
    Auth::routes();
});

Route::get('/@{user}', 'ProfileController@show')->name('profile.show');
Route::get('/@{user}/edit', 'ProfileController@edit')->name('profile.edit');

Route::get('/notifications', 'NotificationController@index')->name('notifications');

Route::prefix('/conversations')->namespace('Conversations')->name('conversation.')->middleware('auth')->group(function () {
    Route::get('/', 'ConversationController@index')->name('index');
    Route::get('/list', 'ConversationController@get_conversations')->name('list');
    Route::get('/new', 'ConversationController@new')->name('new');
    Route::get('/{user}/messages', 'ConversationController@messages')->name('messages');
    Route::post('/{user}/send', 'ConversationController@send')->name('send');
});

Route::prefix('/settings')
        ->name('settings.')
        ->middleware(['auth'])
        ->namespace('Settings')
        ->group(function () {

    Route::get('/', function () {
        return redirect()->route('settings.profile');
    });

    Route::get('/profile', 'ProfileSettingsController@edit')->name('profile'); // profile picture, biography, website ...
    Route::patch('/profile', 'ProfileSettingsController@update')->name('profile.update'); // save

    Route::get('/password', 'PasswordSettingsController@edit')->name('password');
    Route::patch('/password', 'PasswordSettingsController@update')->name('password.update');

    // Route::get('/account', 'SettingsController@index')->name('profile'); // username, deactivate account
    // Route::get('/privacy', 'SettingsController@privacy')->name('settings.privacy');
    // Route::get('/notifications', 'SettingsController@notifications')->name('settings.notifications');
    // Route::resource('/deactivate', 'SettingsController')->only(['index', 'delete']);

});

Route::prefix('/discussions')->namespace('Discussions')->group(function () {
    Route::get('/{category?}', 'DiscussionController@index')->name('discussion.index'); // category is optional
    Route::get('/{category}/create', 'DiscussionController@create')->name('discussion.create');
    Route::post('/{category}', 'DiscussionController@store')->name('discussion.store');
    Route::get('/{category}/{discussion}', 'DiscussionController@show')->name('discussion.show');
    Route::get('/{category}/{discussion}/edit', 'DiscussionController@edit')->name('discussion.edit');
    Route::patch('/{category}/{discussion}', 'DiscussionController@update')->name('discussion.update');
    Route::delete('/{category}/{discussion}', 'DiscussionController@destroy')->name('discussion.delete');

    Route::post('/{category}/{discussion}/likes', 'DiscussionLikeController@store')->name('discussion.like.store');
    Route::delete('/{category}/{discussion}/likes', 'DiscussionLikeController@destroy')->name('discussion.like.destroy');

    Route::post('/{category}/{discussion}/lock', 'DiscussionLockController@store')->name('discussion.lock.store');
    Route::delete('/{category}/{discussion}/lock', 'DiscussionLockController@destroy')->name('discussion.lock.destroy');

    Route::post('/{category}/{discussion}/pin', 'DiscussionPinController@store')->name('discussion.pin.store');
    Route::delete('/{category}/{discussion}/pin', 'DiscussionPinController@destroy')->name('discussion.pin.destroy');

    Route::get('/search', 'DiscussionSearchController@search')->name('discussion.search');

    Route::prefix('/{category}/{discussion}/replies')->namespace('Replies')->group(function () {
        Route::post('/{reply?}', 'DiscussionReplyController@store')->name('discussion.reply.store');
        Route::post('/{reply}/likes', 'DiscussionReplyLikeController@store')->name('discussion.reply.like.store');
        Route::delete('/{reply}/likes', 'DiscussionReplyLikeController@delete')->name('discussion.reply.like.delete');

        Route::post('/{reply}/best', 'DiscussionBestReplyController@store')->name('discussion.reply.best.store');
    });
});

// admin :
Route::prefix('/admin')->name('admin.')->namespace('Admin')->middleware(['auth', 'role:admin'])->group(function () {

    Route::get('/', 'AdminController@index')->name('index');

    Route::resource('users', 'UserController'); // names van 'resource' routes worden automatisch gegenereerd

    Route::get('/invite-users', 'UserInviteController@form')->name('invite-users');
    Route::post('/invite-users', 'UserInviteController@send_invites')->name('invite-users.send');

    Route::resource('projects', 'ProjectController');

    Route::resource('categories', 'CategoryController');

});
